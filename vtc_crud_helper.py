import hashlib


def makeDatabaseURI(conf):
    return conf['DB_TYPE'] + '://' + \
           conf['DB_LOGIN'] + ':' + \
           conf['DB_PASSWD'] + '@' + \
           conf['DB_HOST'] + ':' + \
           str(conf['DB_PORT']) + '/' + \
           conf['DB_NAME']


def readFileContent(path_to_file):
    content = ''
    with open(path_to_file, 'r') as content_file:
        content = content_file.read()
    return content


def sha256(input_str):
    h = hashlib.sha256()
    h.update(bytes(input_str, encoding='utf-8'))
    return h.hexdigest()
