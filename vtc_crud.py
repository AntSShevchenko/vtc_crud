from flask import Flask, request, \
     session, redirect, url_for
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker
import jwt
from datetime import datetime, timedelta, timezone
import json
import re
import os

from vtc_crud_model import User, Token, Film, REGEX_PHONE, REGEX_EMAIL
from vtc_crud_helper import makeDatabaseURI, readFileContent, sha256


api_prefix = '/api'
using_dev_server = (__name__ == '__main__')
if using_dev_server:
    static_url_path = '/'
    static_folder = 'public'
else:
    static_url_path = None
    static_folder = 'static'


app = Flask(__name__, static_url_path=static_url_path, static_folder=static_folder)
app.config.from_pyfile('./vtc_crud.conf')
app.debug = using_dev_server
app.config['ENV'] = 'development' if app.debug else 'production'
app.config['JWT_EXPIRATION_DELTA'] = \
    timedelta(minutes=app.config['JWT_ACCESS_TOKEN_LIFITIME_IN_MINUTES'])
sign_conf = {
    'algo': app.config.get('JWT_ALGORITHM', 'HS256'),
    'nbf_delta': app.config.get('JWT_NOT_BEFORE_DELTA', timedelta(seconds=0)),
    'exp_delta': app.config.get('JWT_EXPIRATION_DELTA', timedelta(minutes=10))
}
if sign_conf['algo'].startswith('HS'):
    sign_conf['sign_key'] = app.config['JWT_SECRET_KEY']
    sign_conf['verify_key'] = app.config['JWT_SECRET_KEY']
else:
    sign_conf['sign_key'] = app.config['JWT_PRIVATE_KEY']
    sign_conf['verify_key'] = app.config['JWT_PUBLIC_KEY']

engine = create_engine(makeDatabaseURI(app.config), echo=app.debug)
Session = sessionmaker(bind=engine)
session = Session()


@app.route('/', methods=['GET'])
@app.route('/login', methods=['GET'])
@app.route('/register', methods=['GET'])
@app.route('/films', methods=['GET'])
def root():
    access_token, error = getValidToken()
    index_file = os.path.join(os.path.dirname(__file__), 'public/index.html')
    index_page = readFileContent(index_file)
    if error is None:
        index_page = re.sub(
            '<div id=.?app.?>',
            '<div id="app" data-auth="' + access_token + '">',
            index_page
        )
    return (
        index_page,
        200,
        {'Content-type': 'text/html; charset=utf-8'}
    )


@app.route(api_prefix + '/signup', methods=['POST'])
def signup():
    global sign_conf
    data_uploaded = request.get_json(force=True)
    login = data_uploaded.get('login')
    passwd = data_uploaded.get('password')
    if login is None or passwd is None:
        return makeErrorResponse(10, 'login or password not present', 400)

    if re.match(REGEX_PHONE, login) is None and \
       re.match(REGEX_EMAIL, login) is None:
        return makeErrorResponse(
            130,
            'user login must be a phone number or an email', 200
        )

    user = session.query(User).filter_by(login=login).first()
    if user is not None:
        return makeErrorResponse(
            131,
            'login already registred, use another email or phone number as login',
            200
        )

    user = User(login, passwd)
    session.add(user)
    session.commit() # oh, else we'll not receive user_id

    access_token = Token(user, sign_conf)
    session.add(access_token)
    session.commit()
    return makeResponse({'access_token': access_token.getJWT()})


@app.route(api_prefix + '/signin', methods=['POST'])
def signin():
    global sign_conf
    data_uploaded = request.get_json(force=True)
    login = data_uploaded.get('login')
    passwd = data_uploaded.get('password')
    if login is None or passwd is None:
        return makeErrorResponse(11, 'login or password not present', 400)

    user = session.query(User).filter_by(login=login).first()
    if user is None:
        return makeErrorResponse(141, 'user not found', 401)

    if user.passwd_hash != sha256(passwd):
        return makeErrorResponse(142, 'password not valid', 401)

    access_token = Token(user, sign_conf)
    session.add(access_token)
    session.commit()
    return makeResponse({'access_token': access_token.getJWT()})


@app.route(api_prefix + '/logout', methods=['GET'])
def logout():
    token, error = getValidTokenPayload()
    if error is not None: return error

    deleted = session.query(Token).\
                      filter(Token.user_id == token['sub']).\
                      delete(synchronize_session=False)
    session.commit()
    return makeResponse({'deleted': deleted})


@app.route(api_prefix + '/info', methods=['GET'])
def info():
    token, error = getValidTokenPayload()
    if error is not None: return error
    return makeResponse({'data': token})


@app.route(api_prefix + '/list', methods=['GET'])
def films():
    token, error = getValidTokenPayload()
    if error is not None: return error

    p_page = request.args.get('page')
    p_limit = request.args.get('limit')
    p_nat_title = request.args.get('nat_title')
    p_loc_title = request.args.get('loc_title')
    p_year_from = request.args.get('year_from')
    p_year_to = request.args.get('year_to')
    p_sort = request.args.get('sort')

    q_films = session.query(Film).\
                      filter(Film.user_id == token['sub'])
    if p_year_from is not None and p_year_from.isdigit():
        q_films = q_films.filter(Film.year >= p_year_from)
    if p_year_to is not None and p_year_to.isdigit():
        q_films = q_films.filter(Film.year <= p_year_to)
    if p_nat_title is not None:
        q_films = q_films.filter(Film.native_title.like('%' + p_nat_title + '%'))
    if p_loc_title is not None:
        q_films = q_films.filter(Film.localized_title.like('%' + p_loc_title + '%'))
    total_count = q_films.count()
    p_desc = False
    if type(p_sort) is str and len(p_sort) > 0:
        if p_sort[0] == '-':
            p_desc = True
            p_sort = p_sort[1:]
        elif p_sort[0] == '+':
            p_sort = p_sort[1:]
        if hasattr(Film, p_sort):
            if p_desc:
                q_films = q_films.order_by(getattr(Film, p_sort).desc())
            else:
                q_films = q_films.order_by(getattr(Film, p_sort))
    if type(p_page) is str and p_page.isdigit() and \
       type(p_limit) is str and p_limit.isdigit():
        q_films = q_films.offset((int(p_page) - 1) * int(p_limit)).limit(int(p_limit))
    films = q_films.all()
    items = []
    for film in films:
        items.append(film.toDict())

    return makeResponse({'total': total_count, 'items': items})


@app.route(api_prefix + '/remove/<int:film_id>', methods=['DELETE'])
def remove(film_id):
    token, error = getValidTokenPayload()
    if error is not None: return error

    deleted = session.query(Film).\
                      filter(Film.id == film_id).\
                      filter(Film.user_id == token['sub']).\
                      delete()
    session.commit()
                      
    if deleted == 0:
        return makeErrorResponse(150, 'film not found or not owned by current user', 404)

    return '', 204


@app.route(api_prefix + '/update/<int:film_id>', methods=['POST'])
def update(film_id):
    token, error = getValidTokenPayload()
    if error is not None: return error

    film = session.query(Film).filter_by(id=film_id).first()
    if film is None:
        return makeErrorResponse(160, 'film not found', 404)
    if film.user_id != token['sub']:
        return makeErrorResponse(161, 'film not owned by current user', 401)

    data_uploaded = request.get_json(force=True)
    p_native_title = data_uploaded.get('native_title')
    p_localized_title = data_uploaded.get('localized_title')
    p_year = data_uploaded.get('year')
    if p_native_title is None:
        return makeErrorResponse(162, 'native film\'s title not present', 400)
    if p_year is not None:
        if not type(p_year) is int:
            return makeErrorResponse(163, 'year filed must be a numeric or not present', 400)
        cur_year = datetime.now().year
        if p_year > 0 and (p_year < 1895 or p_year > cur_year):
            return makeErrorResponse(
                164, 'year must be between 1895 and {cur_year}'.format(cur_year=cur_year), 400
            )
        film.year = p_year
    film.native_title = p_native_title
    if p_localized_title is not None:
        film.localized_title = p_localized_title
    session.commit()
    return '', 204


@app.route(api_prefix + '/create', methods=['POST'])
def create():
    token, error = getValidTokenPayload()
    if error is not None: return error

    data_uploaded = request.get_json(force=True)
    p_native_title = data_uploaded.get('native_title')
    p_localized_title = data_uploaded.get('localized_title')
    p_year = data_uploaded.get('year')
    if p_native_title is None:
        return makeErrorResponse(162, 'native film\'s title not present', 400)
    if p_year is not None:
        if not type(p_year) is int:
            return makeErrorResponse(163, 'year filed must be a numeric or not present', 400)
        cur_year = datetime.now().year
        if p_year > 0 and (p_year < 1895 or p_year > cur_year):
            return makeErrorResponse(
                164, 'year must be between 1895 and {cur_year}'.format(cur_year=cur_year), 400
            )
        if p_year == 0:
            p_year = None
    film = Film(token['sub'], p_native_title, localized_title=p_localized_title, year=p_year)
    session.add(film)
    session.commit()
    return '', 204


def getValidTokenPayload():
    return getValidToken(unpack_payload=True)


def getValidToken(unpack_payload=False):
    error = None
    access_token, error = getTokenFromRequest()
    if error is not None: return None, error

    token_payload, error = verifyJWT(access_token)
    if error is not None: return None, error

    return token_payload if unpack_payload else access_token, None


def getTokenFromRequest():
    token_str = request.headers.get('Authorization')
    if token_str is None:
        return None, makeErrorResponse(20, 'Token not present', 401)
    if not token_str.lower().startswith('bearer '):
        return None, makeErrorResponse(21, "Bad token prefix, must be 'Bearer'", 400)
    return token_str[7:], None


def verifyJWT(jwt_encoded):
    global sign_conf
    error = None
    try:
        token_payload = jwt.decode(jwt_encoded,
                                   sign_conf['verify_key'], sign_conf['algo'])
    except Exception as err:
        error = str(err)
    if error is not None:
        return None, makeErrorResponse(23, error, 401)

    if token_payload.get('sub', None) is None or\
       token_payload.get('jti', None) is None or\
       token_payload.get('nbf', None) is None or\
       token_payload.get('exp', None) is None:
        return None, makeErrorResponse(24, 'sub, jti, nbf and exp token claims are mandatory ', 200)

    found = session.query(Token).filter(Token.id == token_payload['jti']).count()
    if not found:
        return None, makeErrorResponse(22, 'token revoked', 401)

    return token_payload, None


def makeErrorResponse(code, message, http_status):
    result = {"error": {
        "code": code,
        "message": message
    }}
    return makeResponse(result, http_status=http_status)


def makeResponse(values_as_dict, http_status=200):
    return (
        json.dumps(values_as_dict, sort_keys=True),
        http_status,
        {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    )


if __name__ == '__main__':
    app.run()
