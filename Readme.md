## Описание

Это тестовая разработка, целью которой является освоение Python + Flask + SQLAlchemy + PostgreSQL на стороне сервера и Vue.js + vue-i18n + ElementUI на клиенте.

Реализовано приложение — личный каталог кинофильмов с аутентификацией. Пользователь создаёт себя сам на странице регистрации, задав логин и пароль. Логин — номер мобильного телефона с кодом страны или e-mail. При дальнейших визитах пользователь аутентифицируется этими логином и паролем.

В списке фильмов реализован функционал просмотра, добавления, редактирования и удаления фильма.

После регистрации и после каждого логина пользователь получает JWT, которым аутенифицируются дальнейшие запросы к бэку. По устаревании токена (срок жизни ― 5 минут) при любом запросе к бэку пользователь должен снова ввести логин и пароль к форме логина. Refresh-токены не используются.

Ролевой модели нет: каждый пользователь ведёт свой список фильмов. Профиль пользователя не ведётся, хранятся лишь логин и 

При установке БД наполняется тестовыми данными: создаётся один пользователь и его список из 14 фильмов.  
Логин: 79123456789  
Пароль: asdf

## Требования к окружению
### для работы приложения
* Python 3.4+
* модули Python (если не установлены):
    * Flask
    * SQLAlchemy
    * psycopg2
    * PyJWT
* PostgreSQL 9.2+
* Apache HTTPD 2.4+ (только если запуск через apache2)
* mod_wsgi (только если запуск через apache2)

### для разработки клиента
Приложение поставлется с собранным клиентом. Указанное ниже ПО нужно для внесения изменений.

* Node.js (проверено с версиями 8.17.0 и 12.13.1)
* yarn

## Установка
```bash
cd setup
# пароль нового пользователя: crud
sudo -u postgres createuser -d -l -P crud
sudo -u postgres createdb -E UTF8 -l ru_RU.UTF-8 -O crud crud
psql -U crud -f scheme.sql crud
```

## Запуск
Запуск под управлением сервера разработки Flask:

`python3 vtc_crud.py`

Также возможен запуск под управлением Apache HTTPD + mod_wsgi (см. apache_conf/wsgi.conf).



## TODO:
* проверять устаревание токена в клиенте до запроса к бэку;
* асимметричная криптография в JWT;
* сохранение редактируемого фильма по нажатию Enter;
* коррекция работы с сессией БД в бэке;
* исключение лишних SVG из клиента;
* исключение лишних стилей из клиента;
* хранение списка фильмов перенести во vuex;
* сохранение редактируемых данных при перелогине из-за устаревания токена;
* при сборке клиента разбивать chunk-vendors.js на мелкие составляющие.
