import sys
import os

root_dir = os.path.dirname(__file__)
sys.path.insert(0, root_dir)
from vtc_crud import app as application
