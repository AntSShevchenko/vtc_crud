import request from './request';

export interface IListQuery {
  page: number;
  limit: number;
  nat_title: string | undefined;
  loc_title: string | undefined;
  year_from: number | undefined;
  year_to: number | undefined;
  sort: string;
}

export interface IFilm {
  id: number;
  native_title: string;
  localized_title?: string;
  year: number | null;
}

export const list = (params: IListQuery): Promise<any> =>
  request({
    url: '/list',
    method: 'get',
    params,
  });

export const remove = (id: number): Promise<any> =>
  request({
    url: `/remove/${id}`,
    method: 'delete',
  });

export const update = (params: IFilm): Promise<any> => {
  const {id, ...data} = params;
  return request({
    url: `/update/${id}`,
    method: 'post',
    data,
  });
}

export const create = (params: IFilm): Promise<any> => {
  const {id, ...data} = params;
  if (data.localized_title === '') {
    delete data.localized_title;
  }
  if (data.year === 0) {
    delete data.year;
  }
  return request({
    url: '/create',
    method: 'post',
    data,
  });
}
