import axios from 'axios';
import { Message, MessageBox } from 'element-ui';
import { vxmApp } from '@/store/modules/app';
import { vxmUser } from '@/store/modules/user';
import i18n from '@/lang';
import router from '../router';

const service = axios.create({
  baseURL: '/api', // url = base url + request url
  timeout: 60000,
  maxContentLength: 1000000,
});

// Request interceptors
service.interceptors.request.use(
  (config) => {
    if (vxmUser.token) {
      config.headers.Authorization = 'Bearer ' + vxmUser.token;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

// Response interceptors
service.interceptors.response.use(
  (response) => {
    // Error codes:
    //  10, 'login or password not present', 400
    //  11, 'login or password not present', 400

    //  20, 'Unauthorized', 401 token not present
    //  21, "Bad token prefix, must be 'Bearer'", 400
    //  22, 'token revoked', 401
    //  23, JWT-decode-or-verify-error, 401 (may be, token expired)
    //  24, 'sub, jti, nbf and exp token claims are mandatory ', 401

    // 130, 'user login must be a phone number or an email', 200
    // 131, 'login already registred, use another email or phone number as login', 200

    // 141, 'user not found', 401
    // 142, 'password not valid', 401

    // 150, 'film not found or not owned by current user', 404
    const res = response.data;
    if (typeof res.error !== 'undefined') {
      // tslint:disable-next-line:no-console
      console.error('error:', JSON.stringify(res.error));
      displayErrorMessage(res.error);
      if (isBadTokenAuthentication(res.error)) {
        vxmUser.resetToken();
        router.push({name: 'login'});
      }
    }
    return res;
  },
  (error) => {
    if (typeof error.response !== 'undefined' &&
        typeof error.response.data !== 'undefined' &&
        typeof error.response.data.error !== 'undefined') {
      // tslint:disable-next-line:no-console
      console.error('error:', JSON.stringify(error.response.data.error));
      displayErrorMessage(error.response.data.error);
      if (isBadTokenAuthentication(error.response.data.error)) {
        vxmUser.resetToken();
        router.push({name: 'login'});
      }
      return error.response.data;
    } else {
      displayErrorMessage(undefined);
      Promise.reject(error);
    }
  },
);

function displayErrorMessage(errorData: any): void {
  let message = '';
  let msgPath = '';
  if (typeof errorData === 'undefined') {
    message = i18n.t('apiError.unknown', vxmApp.language).toString();
  } else if (typeof errorData.code === 'number') {
    msgPath = 'apiError.error' + errorData.code.toString();
    message = i18n.t(
      msgPath,
      vxmApp.language,
    ).toString();
  }
  if (!message || message === msgPath) {
    if (typeof errorData.message === 'string') {
      message = errorData.message;
    }
  }
  Message({
    message: message || 'Error',
    type: 'error',
    duration: 5 * 1000,
  });
}

function isBadTokenAuthentication(errorData: any): boolean {
  if (typeof errorData.code === 'number') {
    if (errorData.code > 20 && errorData.code < 30) {
      return true;
    }
  }
  return false;
}

export default service;
