import request from './request';

export const signin = (username: string, password: string): Promise<any> =>
  request({
    url: '/signin',
    method: 'post',
    data: { login: username.trim(), password },
  });

export const signup = (username: string, password: string): Promise<any> =>
request({
  url: '/signup',
  method: 'post',
  data: { login: username.trim(), password },
});

export const logout = (): Promise<any> =>
request({
  url: '/logout',
  method: 'get',
});
