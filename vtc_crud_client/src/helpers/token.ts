import * as base64json from 'base64json';

export function getPreloadedToken(): string {
  const di = document.getElementById('app');
  let token = (di ? (di.dataset ? (di.dataset.auth ? di.dataset.auth : '') : '') : '');

  if (!token) {
    token = window.localStorage.getItem('access_token') || '';
  }

  return token;
}

export function storeTokenInBrowser(token: string): void {
  if (token) {
    window.localStorage.setItem('access_token', token);
  }
}

export function removeToken(): void {
  window.localStorage.removeItem('access_token');
}

export function getTokenPayload(token: string): any {
  try {
    if (typeof token !== 'string' || token.split('.').length !== 3) {
      return null;
    } else {
      const payload = token.split('.')[1];
      return base64json.parse(payload);
    }
  } catch (error) {
    return null;
  }
}

export function isTokenExpired(token: string | any): boolean {
  let payload: any = {};
  if (typeof token === 'string') {
    payload = getTokenPayload(token) || {};
  } else if (typeof token === 'object' && token !== null) {
    payload = token;
  }
  if (typeof payload.exp !== 'number') {
    return false;
  }
  const expirationDate: Date = new Date(0);
  expirationDate.setUTCSeconds(payload.exp);

  return expirationDate.valueOf() < new Date().valueOf();
}

