const REGEX_PHONE = '^[0-9]{11,13}$';
const REGEX_EMAIL = '^[-_0-9A-Za-z]+@([-_0-9A-Za-z]+\.)+[A-Za-z]{2,}$';

export function isValidLogin(login: string): boolean {
  const rePhone = new RegExp(REGEX_PHONE);
  const reEmail = new RegExp(REGEX_EMAIL);
  return rePhone.test(login) || reEmail.test(login);
}
