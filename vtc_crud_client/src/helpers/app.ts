const localeKey = 'lang';
export const setLocale = (language: string) => localStorage.setItem(localeKey, language);

const sizeKey = 'size';
export const getSize = () => localStorage.getItem(sizeKey);
export const setSize = (size: string) => localStorage.setItem(sizeKey, size);
