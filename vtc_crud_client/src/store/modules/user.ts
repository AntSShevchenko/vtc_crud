import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators';
import store from '@/store';
import {
  storeTokenInBrowser,
  removeToken,
  getTokenPayload,
  isTokenExpired,
} from '@/helpers';
import { signin, signup, logout } from '@/api/auth';
import { getPreloadedToken } from '@/helpers';

export interface IUserState {
  token: string;
  tokenExpire: Date;
  login: string;
}


@Module({ dynamic: true, store, name: 'user' })
class User extends VuexModule implements IUserState {
  public token = '';
  public tokenExpire = new Date(0);
  public login = '';


  @Action
  public loadToken(): void {
    const token: string = getPreloadedToken();
    if (token) {
      this.storeToken(token);
    }
  }

  @Action
  public storeToken(token: string): void {
    const tokenPayload = getTokenPayload(token);
    if (tokenPayload === null) {
      return;
    }
    if (isTokenExpired(tokenPayload)) {
      return;
    }
    this.SET_TOKEN(token);
    storeTokenInBrowser(token);
    if (typeof tokenPayload.login !== 'undefined') {
      this.SET_LOGIN(tokenPayload.login);
    }
    if (typeof tokenPayload.exp === 'number') {
      this.SET_TOKEN_EXPIRE(tokenPayload.exp);
    } else {
      this.SET_TOKEN_EXPIRE(0);
    }
  }

  @Action
  public resetToken(): void {
    removeToken();
    this.SET_TOKEN('');
    this.SET_TOKEN_EXPIRE(0);
    this.SET_LOGIN('');
  }

  @Action
  public async logIn(userInfo: { username: string, password: string}) {
    const { username, password } = userInfo;
    try {
      const data = await signin(username, password);
      if (typeof data.access_token !== 'undefined') {
        this.storeToken(data.access_token);
      }
    } catch (err) { ; }
  }

  @Action
  public async logOut() {
    if (this.tokenExpire > new Date()) {
      await logout();
    }
    this.resetToken();
  }

  @Action
  public async register(userInfo: { username: string, password: string}) {
    const { username, password } = userInfo;
    const data = await signup(username, password);
    if (typeof data.access_token !== 'undefined') {
      this.storeToken(data.access_token);
    }
  }

  @Mutation
  private SET_TOKEN(token: string) {
    this.token = token;
  }

  @Mutation
  private SET_TOKEN_EXPIRE(tokenExpire: number) {
    this.tokenExpire = new Date(tokenExpire * 1000);
  }

  @Mutation
  private SET_LOGIN(login: string) {
    this.login = login;
  }

  /*
  @Action
  public async LogOut() {
    if (this.token === '') {
      throw Error('LogOut: token is undefined!')
    }
    await logout()
    removeToken()
    resetRouter()

    // Reset visited views and cached views
    TagsViewModule.delAllViews()
    this.SET_TOKEN('')
    this.SET_ROLES([])
  }
  */
}

export const vxmUser = getModule(User);
