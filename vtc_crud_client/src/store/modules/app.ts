import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import { setLocale, getSize, setSize } from '@/helpers';
import { getLocale } from '@/lang';
import store from '@/store';

export enum DeviceType {
  Mobile = 'mobile',
  Desktop = 'desktop',
}

export interface IAppState {
  device: DeviceType;
  language: string;
  size: string;
}

@Module({ dynamic: true, store, name: 'app' })
class App extends VuexModule implements IAppState {
  public device = DeviceType.Desktop;
  public language = getLocale();
  public size = getSize() || 'medium';

  @Action
  public setDevice(device: DeviceType) {
    this.SET_DEVICE(device);
  }

  @Action
  public setLanguage(language: string) {
    setLocale(language);
    this.SET_LANGUAGE(language);
  }

  @Action
  public setSize(size: string) {
    setSize(size);
    this.SET_SIZE(size);
  }

  @Mutation
  private SET_DEVICE(device: DeviceType) {
    this.device = device;
  }

  @Mutation
  private SET_LANGUAGE(language: string) {
    this.language = language;
  }

  @Mutation
  private SET_SIZE(size: string) {
    this.size = size;
  }
}

export const vxmApp = getModule(App);
