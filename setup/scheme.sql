create table if not exists crud_user (
    user_id serial not null primary key,
    user_login varchar(50) not null unique,
    passwd_hash varchar(64) not null
);


create table if not exists crud_token (
    token_id serial not null primary key,
    user_id integer not null REFERENCES crud_user (user_id) on delete cascade on update cascade,
    token_expire timestamp with time zone -- for periodical cleaning
);

create index token_expire on crud_token (
    token_expire asc
);

create table if not exists crud_film (
    film_id serial not null primary key,
    user_id integer not null REFERENCES crud_user (user_id) on delete cascade on update cascade,
    native_title varchar(100) not null,
    localized_title varchar(100) not null,
    release_year integer default null
);

create index film_native_title on crud_film (
    native_title asc
);
create index film_localized_title on crud_film (
    localized_title asc
);


insert into public.crud_user (user_login, passwd_hash)
values ('79123456789', 'f0e4c2f76c58916ec258f246851bea091d14d4247a2fc3e18694461b1816e13b');

insert into crud_film (native_title, localized_title, release_year, user_id)
values
('Pride and prejustice', 'Гордость и предубеждение', 1940, 1),
('Pride and prejustice', 'Гордость и предубеждение', 1995, 1),
('Pride and prejustice', 'Гордость и предубеждение', 2005, 1),
('Tremors', 'Дрожь земли', 1990, 1),
('You''ve Got Mail', 'Вам письмо', 1998, 1),
('Sleepless in Seattle', 'Неспящие в Сиэтле', 1993, 1),
('Legends of the Fall', 'Легенды осени', 1994, 1),
('Moonrise Kingdom', 'Королевство полной луны', 2012, 1),
('French Kiss', 'Французский поцелуй', 1995, 1),
('Joe Versus The Volcano', 'Джо против вулкана', 1990, 1),
('Miss Pettigrew Lives for a Day', 'Мисс Петтигрю живёт одним днём', 2008, 1),
('Miss Peregrine''s Home for Peculiar Children', 'Дом странных детей мисс Перегрин', 2016, 1),
('Завтрак на траве', 'Завтрак на траве', 1979, 1),
('В бой идут одни старики', 'В бой идут одни старики', 1973, 1)
;