from sqlalchemy import Column, Integer, String, Boolean, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime, timedelta, timezone
import jwt
import re

from vtc_crud_helper import sha256


REGEX_PHONE = '^[0-9]{11,13}$'
REGEX_EMAIL = '^[-_0-9A-Za-z]+@([-_0-9A-Za-z]+\.)+[A-Za-z]{2,}$'

Base = declarative_base()


class User(Base):
    __tablename__ = 'crud_user'
    id = Column('user_id', Integer, primary_key=True, nullable=False)
    login = Column('user_login', String(50), unique=True, nullable=False)
    passwd_hash = Column(String(64), nullable=False)

    def __init__(self, user_login, pure_password):
        self.login = user_login
        self.passwd_hash = sha256(pure_password)

    def __repr__(self):
        if self.id is not None:
            return "<User('{id}, {login}')>".format(id=self.id, login=self.login)
        else:
            return "<User({login})>".format(login=self.login)


class Token(Base):
    __tablename__ = 'crud_token'
    id = Column('token_id', Integer, primary_key=True, nullable=False)
    user_id = Column(Integer, ForeignKey('crud_user.user_id'), nullable=False)
    token_expire = Column(DateTime(timezone=True))

    def __init__(self, user, conf):
        self.user_id  = user.id
        self.login = user.login
        self.iat = datetime.now(tz=timezone.utc)
        self.nbf = self.iat + conf['nbf_delta']
        self.token_expire = self.nbf + conf['exp_delta']
        self.algo = conf['algo']
        self.sign_key = conf['sign_key']

    def __repr__(self):
        if self.id is not None:
            return "<JWT('{id}, {user_id}, {exp}')>".format(
                id=self.id, user_id=self.user_id, exp=str(self.token_expire)
            )
        else:
            return "<JWT({user_id}, {exp}')>".format(
                user_id=self.user_id, exp=str(self.token_expire)
            )

    def getJWT(self):
        return jwt.encode(
            {
                'iss': 'crud-auth-service',
                'sub': self.user_id,
                'login': self.login,
                'jti': self.id,
                'iat': int(self.iat.timestamp()),
                'nbf': int(self.nbf.timestamp()),
                'exp': int(self.token_expire.timestamp())
            },
            self.sign_key,
            self.algo
        ).decode('UTF8')


class Film(Base):
    __tablename__ = 'crud_film'
    id = Column('film_id', Integer, primary_key=True, nullable=False)
    user_id = Column(Integer, ForeignKey('crud_user.user_id'), nullable=False)
    native_title = Column(String(100), unique=False, nullable=False)
    localized_title = Column(String(100), unique=False, nullable=False)
    year = Column('release_year', Integer, unique=False, nullable=True)

    def __init__(self, user_id, native_title, localized_title=None, year=None):
        self.user_id = user_id
        self.native_title = native_title
        self.localized_title = localized_title if localized_title is not None else native_title
        self.year = year

    def __repr__(self):
        year_str = str(self.year) if self.year is not None else ''
        if self.id is not None:
            return "<Film('{id}, {title}, {year}')>".format(
                id=self.id, title=self.native_title, year=year_str
            )
        else:
            return "<Film('{title}, {year}')>".format(
                title=self.native_title, year=year_str
            )

    def toDict(self):
        return {
            'id': self.id,
            'native_title': self.native_title,
            'localized_title': self.localized_title,
            'year': self.year
        }